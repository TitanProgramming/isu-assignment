﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Attendance_ISU_program
{
    public partial class newStudentForm : Form
    {
        //declaring the variables
        string name = "";
        int IDnumber = 0;


        public newStudentForm()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //check if there was a name entered
            //if so set the name to a variable
            if (txtStudentName.Text == "")
            {
                //display an error message box
                MessageBox.Show("Please enter a name into the InputBox.",
                    "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                //set the value of the name
                name = txtStudentName.Text;

                //send the value to the form and close this form
                mainForm.newName = name;
                this.Close();
            }
            if (txtStudentNumber.Text == "")
            {
                MessageBox.Show("Please enter a unique student ID","Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else
            {
                try
                {

                    IDnumber = int.Parse(txtStudentNumber.Text);
                }

                catch
                {
                    MessageBox.Show("Please enter a unique student ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }

            Student student1 = new Student();
                student1.Name = name;
                student1.StudentNumber = IDnumber;
            student1.RoomNumber = new string [5] ;
            student1.Status = new string [5];
            student1.Teacher = new string [5];

            for (int i = 0; i < 5; i++)
            {
                student1.RoomNumber [i] = "";
                student1.Status[i] = "";
                student1.Teacher[i] = "";
            }

             student1.Store ();
    
        }

        private void newStudentForm_Load(object sender, EventArgs e)
        {

        }

    }
}
