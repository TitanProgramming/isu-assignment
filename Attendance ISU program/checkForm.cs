﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
namespace Attendance_ISU_program
{
    public partial class checkForm : Form
    {
        public checkForm()
        {
            InitializeComponent();
        }

        private void btnTestData_Click(object sender, EventArgs e)
        {
           
        }

        private void checkForm_Load(object sender, EventArgs e)
        {
            //create all your rows and cells 
            this.studentData.Rows.Add("Room", "", "", "", "");
            this.studentData.Rows.Add("Teacher", "", "", "", "");
            this.studentData.Rows.Add("Status", "", "", "", "");
       
        }

        private void saveText_Click(object sender, EventArgs e)
        {//creates new textfile
            

            
            try
            {

                //string to act as delimiter
                //string sLine = "";
                Student info = new Student ();
                
                info.StudentNumber = 1;
                info.Teacher = new string [5];
                info.RoomNumber = new string [5];
                info.Status = new string [5];
           


                //This for loop loops through each row in the table
                for (int coloumn = 0; coloumn <= studentData.Columns.Count - 2; coloumn++)

                {// sets array for each row = value of cells
                    //+1 for the title coloum, same for -2 in for condition
                    info.RoomNumber[coloumn] = studentData.Rows[0].Cells[coloumn+1].Value.ToString ();

                    info.Teacher[coloumn] = studentData.Rows[1].Cells[coloumn+1].Value.ToString ();

                    info.Status[coloumn] = studentData.Rows[2].Cells[coloumn+1].Value.ToString ();

                }
                //if succesful, closes textfile and  shows messagebox indicating 
                //completion of export

                info.Store();

                System.Windows.Forms.MessageBox.Show("Export Complete.", "Program Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
           catch (System.Exception err) //if exception
            {//shows error messagebox and closes file
                System.Windows.Forms.MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
            }
                
            }
        }
    }

