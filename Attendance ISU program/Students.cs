﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using System.IO;
namespace Attendance_ISU_program
{
    public class Student
    {
        /// <summary>
        /// A unique number for each student, becuase I have one...
        /// </summary>
        /// 
        public string Name;
        public string[] RoomNumber;
        public string[] Status;
        public int StudentNumber;
        public string[] Teacher;

        public void Store()
        {
            StreamWriter sw = new StreamWriter("E:\\Student Info.txt", true);
            sw.WriteLine("Student#: " + StudentNumber.ToString());
            sw.WriteLine("Name: " + Name);

            string RoomNumberLine = null;
            for (int i = 0; i < RoomNumber.Count (); i++)
            {//store RoomNumber
                //adds the value of each cell to the string followed by a comma between every integer. The comma acts as delimiter, 
                //but places a comma at end of string                
                RoomNumberLine += RoomNumber[i] + ",";

            }
            
            string TeacherLine = null;
            for (int i = 0; i < Teacher.Count(); i++)
            {//adds the value of each cell to the string followed by a comma between every integer. The comma acts as delimiter, 
                //but places a comma at end of string 
                TeacherLine += Teacher[i] + ",";
            }

            string StatusLine = null;

            for (int i = 0; i < Status.Count(); i++)
            {//adds the value of each cell to the string followed by a comma between every integer. The comma acts as delimiter, 
                //but places a comma at end of string 
                StatusLine += Status[i] + ",";
            }

            //removes last comma and writes to text file
            sw.WriteLine(RoomNumberLine.Substring(0, RoomNumberLine.Length - 1));
            
            sw.WriteLine(StatusLine.Substring(0, TeacherLine.Length - 1));
            sw.WriteLine(TeacherLine.Substring(0, TeacherLine.Length - 1));


            
            sw.Close();
        }
    }
}
