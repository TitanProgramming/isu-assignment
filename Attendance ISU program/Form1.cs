﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//using the system io
using System.IO;

namespace Attendance_ISU_program
{
    public partial class mainForm : Form
    {
        //declaring the variables
        public static string newName = "";
        public string line = null;

        public mainForm()
        {
            InitializeComponent();
        }

        
        private void listStudentName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //add a student to the list box
            //call the method
            addStudent();
        }

        //ADD STUDENT METHOD
        private void addStudent()
        {
            //show the input box
            newStudentForm studentVar = new newStudentForm();
            studentVar.ShowDialog();

            //add the name to the database if there is a name
            if (newName != "")
            {
                listStudentName.Items.Add(newName);
            }

            //creates a new instance of streamwriter
            //writes to the instance and closes it
            StreamWriter sw = new StreamWriter("E:\\Log.txt", true);

            sw.WriteLine(newName);

            sw.Close();
        }

        private void mainForm_Load(object sender, EventArgs e)
        {
            //FORM LOAD

            //writes a new instance of the test file
            StreamWriter sw = new StreamWriter("E:\\Log.txt", true);
            sw.Close();

            //reads the test file
            StreamReader sr = new StreamReader("E:\\Log.txt", true);

            //Read the first line of text
            line = sr.ReadLine();

            //Continue to read until you reach end of file
            while (line != null)
            {
                //write the lie to console window
                listStudentName.Items.Add(line);
                //Read the next line
                line = sr.ReadLine();
            }

            //close the file
            sr.Close();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
           
        }

        //REMOVE STUDENT METHOD
        private void removeStudent()
        { 
        //remove the currently selected student
            listStudentName.Items.Remove(listStudentName.SelectedItem);
        }

        private void testForm3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //FOR TESTING PURPOSES ONLY
            checkForm checkVar = new checkForm();
            this.Hide();
            checkVar.ShowDialog();
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            checkForm checkForm1 = new checkForm();
            checkForm1.ShowDialog ();
            this.Close ();
        }
    }
}
